﻿using System.Collections.Generic;

namespace IndividualMaps {
  class CourseSetting {
    public string Name { get; set; }
    public List<MapSetting> Maps { get; set; }
  }
}