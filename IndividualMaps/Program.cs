﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Newtonsoft.Json;

namespace IndividualMaps {
  class Program {
    static void Main(string[] args) {
      var events = JsonConvert.DeserializeObject<List<EventSetting>>(File.ReadAllText("maps.json"));

      foreach (var e in events) {
        Console.WriteLine($"Processing {e.Name}");

        var w = new Worker();
        /*
        int extra = e.Extra;
        var persons = w.Persons(e.EntryFile);

        foreach (var map in e.Courses.Select(p => p.Name).Distinct()) {
          int extraCounter = 1;
          for (int i = 0; i < extra; i++) {
            persons.Add(new Runner { Class = "N/A", Course = map, Name = $"Extra {map}.{extraCounter++}", Map = null, Variation = null });
          }
        };

        persons = persons.OrderBy(p => p.Course).ThenBy(p => p.Class).ThenBy(p => p.Name).ToList();

        var pg = persons.GroupBy(p => p.Course);

        foreach (var g in pg) {
          var m = e.Courses.First(p => p.Name == g.Key).Maps;
          var runners = g.ToList();

          int extraWhole = g.Count() % m.Count;
          int whole = g.Count() - extraWhole;

          var maps = new List<MapSetting>();

          for (var i = 0; i < whole; i++) {
            maps.AddRange(m);
          }

          if (extraWhole > 0) {
            maps.AddRange(PickSomeInRandomOrder(m, extraWhole));
          }

          m = RandomOrder(m).ToList();

          for (var i = 0; i < g.Count(); i++) {
            runners[i].Map = m[i % m.Count].File;
            runners[i].Variation = m[i % m.Count].Name;
          }
        }

        File.WriteAllText(e.Mappping, JsonConvert.SerializeObject(persons, Formatting.Indented));

        w.Merge(e.MapFolder, e.TempFile, persons, e);
        w.Stamp(e.TempFile, e.OutFile, persons, e);


        var persons = JsonConvert.DeserializeObject<List<Runner>>(File.ReadAllText(e.Mappping));

        w.WriteXml(e.EntryFile, e.UpdateEntryFile, persons);
                */

        var persons = JsonConvert.DeserializeObject<List<Runner>>(File.ReadAllText(e.Mappping));

        string header = File.ReadAllLines(e.CsvIn).FirstOrDefault();
        File.WriteAllText(e.CsvOut, header);

        using (TextReader textReader = File.OpenText(e.CsvIn)) {
          var csvR = new CsvReader(textReader, new CsvHelper.Configuration.Configuration { Delimiter = ";", HasHeaderRecord = true });
          csvR.Read();
          csvR.ReadHeader();

          using (TextWriter tw = File.AppendText(e.CsvOut)) {
            var csvW = new CsvWriter(tw, new CsvHelper.Configuration.Configuration { Delimiter = ";", QuoteAllFields = true });

            while (csvR.Read()) {
              var row = csvR.GetRecord<dynamic>();
              IDictionary<string, object> expandoDict = row;

              string f = expandoDict["First name"].ToString();
              string l = expandoDict["Surname"].ToString();
              string c = expandoDict["Short"].ToString();

              var item = persons.First(p => p.Name == $"{f} {l}" && p.Class == c);

              row.Course = item.Variation;

              csvW.WriteRecord(row);
              csvW.NextRecord();
            }
          }
        }
      }

      Console.WriteLine("Done!");
      Console.ReadLine();
    }

    static IEnumerable<SomeType> PickSomeInRandomOrder<SomeType>(IEnumerable<SomeType> someTypes, int maxCount) {
      return RandomOrder(someTypes).Take(maxCount);
    }

    static IEnumerable<T> RandomOrder<T>(IEnumerable<T> someTypes) {
      Random random = new Random(DateTime.Now.Millisecond);

      Dictionary<double, T> randomSortTable = new Dictionary<double, T>();

      foreach (var someType in someTypes) {
        randomSortTable[random.NextDouble()] = someType;
      }

      return randomSortTable.OrderBy(p => p.Key).Select(p => p.Value);
    }
  }
}