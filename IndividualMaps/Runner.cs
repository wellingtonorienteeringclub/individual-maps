﻿namespace IndividualMaps {
  class Runner {
    public string Name { get; set; }
    public string Course { get; set; }
    public string Class { get; set; }
    public string Variation { get; set; }
    public string Map { get; set; }
    public string Club { get; set; }
  }
}