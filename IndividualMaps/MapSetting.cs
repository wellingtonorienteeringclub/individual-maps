﻿namespace IndividualMaps {
  class MapSetting {
    public string Name { get; set; }
    public string File { get; set; }
  }
}