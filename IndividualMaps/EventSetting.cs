﻿using System.Collections.Generic;

namespace IndividualMaps {
  class EventSetting {
    public string Name { get; set; }
    public string MapFolder { get; set; }
    public string EntryFile { get; set; }
    public string UpdateEntryFile { get; set; }
    public string OutFile { get; set; }
    public string TempFile { get; set; }
    public string Mappping { get; set; }
    public int Extra { get; set; }
    public bool PrintName { get; set; }
    public List<CourseSetting> Courses { get; set; }
    public string CsvIn { get; set; }
    public string CsvOut { get; set; }
  }
}