﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace IndividualMaps {
  class Worker {
    public List<Runner> Persons(string sourceXmlPath) {
      XmlSerializer serializer = new XmlSerializer(typeof(EntryList));
      StringReader stringReader = new StringReader(File.ReadAllText(sourceXmlPath));
      EntryList root = (EntryList)serializer.Deserialize(stringReader);

      return root.PersonEntry.Select(p => new Runner {
        Class = p.Class[0].Name,
        Course = p.Class[0].RaceClass[0].Course[0].Name,
        Name = p.Person.Name.Given + " " + p.Person.Name.Family,
        Variation = "",
        Map = "",
        Club = p.Organisation.Name
      }).ToList();
    }

    public void WriteXml(string sourceXmlPath, string destinationXmlPath, List<Runner> runners) {
      XmlSerializer serializer = new XmlSerializer(typeof(EntryList));
      StringReader stringReader = new StringReader(File.ReadAllText(sourceXmlPath));
      EntryList root = (EntryList)serializer.Deserialize(stringReader);

      var lookup = runners.ToDictionary(k => $"{k.Name}::{k.Club}::{k.Class}", v => v.Variation);

      foreach (var r in root.PersonEntry) {
        string key = $"{r.Person.Name.Given} {r.Person.Name.Family}::{r.Organisation.Name}::{r.Class[0].Name}";
        r.Class[0].RaceClass[0].Course[0].Name = lookup[key];
      }

      Utf8StringWriter textWriter = new Utf8StringWriter();

      serializer.Serialize(textWriter, root);
      File.WriteAllText(destinationXmlPath, textWriter.ToString());
    }

    public void Merge(string sourceFolder, string destinationFile, List<Runner> runners, EventSetting e) {
      using (MemoryStream stream = new MemoryStream()) {
        Document doc = new Document();
        PdfCopy pdf = new PdfCopy(doc, stream);
        pdf.CloseStream = false;
        doc.Open();

        PdfReader reader = null;
        PdfImportedPage page = null;

        foreach (var runner in runners) {
          var file = Path.Combine(sourceFolder, runner.Map);

          reader = new PdfReader(file);
          for (int i = 0; i < reader.NumberOfPages; i++) {
            page = pdf.GetImportedPage(reader, i + 1);

            Console.WriteLine($"Adding map for {runner.Name}.");
            pdf.AddPage(page);
          }

          pdf.FreeReader(reader);
          reader.Close();
        }

        doc.Close();

        using (FileStream streamX = new FileStream(destinationFile, FileMode.Create)) {
          stream.WriteTo(streamX);
        }
      }
    }

    public void Stamp(string sourceFile, string destinationFile, List<Runner> runners, EventSetting e) {
      PdfReader pdfReader = new PdfReader(sourceFile);

      FileStream stream = new FileStream(destinationFile, FileMode.Create, FileAccess.Write);

      PdfStamper pdfStamper = new PdfStamper(pdfReader, stream);

      BaseFont baseFont = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);

      if (pdfStamper.Reader.NumberOfPages != runners.Count) {
        //throw new Exception("Number of runners not matching number of maps");
      }

      if (e.PrintName) {
        for (int index = 0; index < pdfStamper.Reader.NumberOfPages; index++) {
          Console.WriteLine($"Stamping {runners[index].Name}.");

          PdfContentByte data = pdfStamper.GetOverContent(index + 1);
          var size = pdfReader.GetPageSizeWithRotation(index + 1);

          data.SetColorFill(new Color(255, 255, 255));
          data.Rectangle(500, size.Height - 35, 180, 20);
          data.Fill();

          data.BeginText();
          data.SetColorFill(CMYKColor.DARK_GRAY);
          data.SetFontAndSize(baseFont, 15);
          data.ShowTextAligned(PdfContentByte.ALIGN_LEFT, runners[index].Name + " " + runners[index].Variation, 500, size.Height - 30, 0);
          data.EndText();
        }
      }

      pdfStamper.Close();
      stream.Close();
      pdfReader.Close();
    }
  }
}